package com.example.etest

import android.content.ContentProvider
import android.content.ContentValues
import android.database.Cursor
import android.database.MatrixCursor
import android.net.Uri
import android.os.Bundle

class ImageContentProvider : ContentProvider() {
    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<String>?): Int {
        throw IllegalStateException("delete not allowed")
    }

    override fun getType(uri: Uri): String? {
        return null
    }

    override fun insert(uri: Uri, values: ContentValues?): Uri? {
        throw IllegalStateException("insert not allowed")
    }

    override fun onCreate(): Boolean {
        return true
    }

    override fun query(
        uri: Uri, projection: Array<String>?, selection: String?,
        selectionArgs: Array<String>?, sortOrder: String?
    ): Cursor {
        val repository = ImageRepository(context!!.dataDir!!.path, { }, { })

        val cursor = MatrixCursor(arrayOf("image")).apply {
            extras = Bundle().apply {
                putParcelable("image", repository.bitmap)
            }
        }

        print("Extras: " +  cursor.extras.get("image"))
        return cursor
    }

    override fun update(
        uri: Uri, values: ContentValues?, selection: String?,
        selectionArgs: Array<String>?
    ): Int {
        throw IllegalStateException("update not allowed")
    }
}