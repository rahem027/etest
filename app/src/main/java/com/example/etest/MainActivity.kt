package com.example.etest

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.etest.databinding.ActivityMainBinding
import com.google.android.material.snackbar.Snackbar
import java.lang.Exception

class MainActivity : AppCompatActivity() {

    private val binding: ActivityMainBinding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(binding.root)

        val viewModelFactory = ImageViewModelFactory(dataDir.path, this::onNewBitmap, this::onError)
        val viewModel = ViewModelProvider(this, viewModelFactory).get(ImageRepository::class.java)

        binding.nextPictureButton.setOnClickListener {
            binding.progressBar.visibility = View.VISIBLE
            viewModel.nextImage()
        }
    }

    private fun onNewBitmap(b: Bitmap) {
        this.runOnUiThread {
            binding.progressBar.visibility = View.GONE
            binding.image.setImageBitmap(b)
        }
    }

    private fun onError(e: Exception) {
        binding.progressBar.visibility = View.GONE
        Snackbar.make(
            binding.root,
            R.string.could_not_load_image,
            Snackbar.LENGTH_SHORT,
        ).show()

    }
}