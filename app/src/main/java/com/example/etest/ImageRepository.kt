package com.example.etest

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.io.File
import java.lang.Exception
import java.net.URL
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors


/// We do not need view model in this case and I want to survive
/// configuration changes. That's why I inherited a repository from
/// ViewModel
class ImageRepository(
    baseDir: String,
    /// Bitmap to display changed. **Note: This is called on a background thread.**
    private val onNewBitmap: (Bitmap) -> Unit,
    /// Handle error during bitmap change. **Note: This is called on a background thread.**
    private val onError: (Exception) -> Unit,
    /// I need bitmapStream in init. that's why its declared here.
) :
    ViewModel() {

    private var currentIndex: Int = -1
    private val executor: ExecutorService = Executors.newCachedThreadPool()

    private val filePath = "$baseDir/$fileName"

    var bitmap: Bitmap? = null

    init {
        val file = File(filePath)
        if (file.exists()) {
            this.bitmap = BitmapFactory.decodeFile(file.path)
            onNewBitmap(this.bitmap!!)
        }
    }

    private val urls = listOf(
        "https://i.picsum.photos/id/1002/4312/2868.jpg?hmac=5LlLE-NY9oMnmIQp7ms6IfdvSUQOzP_O3DPMWmyNxwo",
        "https://i.picsum.photos/id/100/2500/1656.jpg?hmac=gWyN-7ZB32rkAjMhKXQgdHOIBRHyTSgzuOK6U0vXb1w",
        "https://i.picsum.photos/id/1000/5626/3635.jpg?hmac=qWh065Fr_M8Oa3sNsdDL8ngWXv2Jb-EE49ZIn6c0P-g",
        "https://i.picsum.photos/id/866/200/300.jpg?hmac=rcadCENKh4rD6MAp6V_ma-AyWv641M4iiOpe1RyFHeI"
    )



    fun nextImage() {
        currentIndex++
        executor.submit {
            try {
                val bitmap = bitmapFromUrl(urls[currentIndex % urls.size])
                this.bitmap = bitmap
                onNewBitmap(bitmap)
            } catch (e: Exception) {
                onError(e)
            }

        }
    }

    private fun bitmapFromUrl(url: String): Bitmap {
        val inputStream = URL(url).openStream()
        val file = File(filePath)
        file.writeBytes(inputStream.readBytes())
        inputStream.close()

        return BitmapFactory.decodeFile(file.path)
    }

}

class ImageViewModelFactory(
    private val baseDir: String,
    private val onNewBitmap: (Bitmap) -> Unit,
    private val onError: (Exception) -> Unit
) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return ImageRepository(baseDir, onNewBitmap, onError) as T
    }
}